#!/usr/bin/env bash
. config.sh
aws ec2 start-instances --instance-ids $EC2_INSTANCE_ID
