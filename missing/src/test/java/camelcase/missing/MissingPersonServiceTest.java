package camelcase.missing;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;

public class MissingPersonServiceTest
{
	
	@Test
	public void testSearch() throws Exception
	{
		MissingPersonService service = new MissingPersonService();
		service.afterPropertiesSet();
		List<PersonDTO> persons = service.getPersons();
		assertThat( persons ).hasSize( 1000 );
		List<PersonDTO> persons2 = service.getPersons( "Aubrette Reame" );
		assertThat( persons2 ).isNotEmpty();
		assertThat(persons2).hasSize( 2 );
	}

}
