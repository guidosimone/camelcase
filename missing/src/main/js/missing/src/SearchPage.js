import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';


import Typography from "@material-ui/core/Typography/Typography";
import Divider from "@material-ui/core/Divider/Divider";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import TextField from "@material-ui/core/TextField/TextField";
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import Search from "@material-ui/icons/Search";
import Button from "@material-ui/core/Button/Button";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import List from "@material-ui/core/List/List";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
});


class SearchPage extends React.Component {
  state = {
    searchParam: "",
    response: null,
    page: 0
  };

  searchDidChange = (e) => {
    const id = e.target.id || e.target.name;
    const value = e.target.value;
    this.setState({
      [id]: value
    });

  };
  doPrevious = () => {
    const page = this.state.page - 1;
    this.doSearch(page);

  };
  doNext = () => {
    const page = this.state.page + 1;
    this.doSearch(page);
  };

  resetAndSearch = (event) => {
    event.preventDefault();
    this.doSearch(0);
  };
  doSearch = (page) => {
    console.log("doSearch");
    let url = "http://" + window.location.host +
        "/person?page=" + page;
    if (this.state.searchParam) {
      url += "&searchParam=" + encodeURIComponent(this.state.searchParam);
    }

    this.setState({
      response: null,
      page
    });
    fetch(url, {
      method: 'GET', // or 'PUT'
      headers: new Headers({
        'Accept': 'application/json'
      })
    }).then(res => {
      return res.json()
    }).catch(error => {
      console.error('Error:', error)
    }).then(response => {
      console.log('Success:', response)
      this.setState({response})
    });

  };

  renderResponse = () => {
    if (!this.state.response)
      return null;
    const response = this.state.response;
    if (!(response.content && response.content.length))
      return null;
    const content = response.content;

    const result = (<List>
      {content.sort((a, b) => b.missingSince.localeCompare(a.missingSince))
          .map((item, index) => {
            const primaryText = item.firstName + ' ' + item.lastName + " (" + item.gender + ")";
            const secondaryText = "Missing since " + item.missingSince.slice(0, 10) + "; Call " + item.contactPhone;
            return <div key={index}><ListItem dense button>
              {item.image && <Avatar src={item.image}/>}
              <ListItemText primary={primaryText} secondary={secondaryText}/>
            </ListItem>
              <Divider/>
            </div>
          })}
    </List>);
    return result;
  };

  render() {
    const {classes} = this.props;
    const waiting = false;
    const response = this.state.response;
    // response has boolean first/last fields.  Only enable if these
    // booleans exist and the appropriate one is set to false
    const enabledPrevious = response && response.first === false;
    const enabledNext = response && response.last === false;
    return <div>
      <Typography variant={"headline"}>Search For Missing People</Typography>
      {waiting ? (<LinearProgress variant={"query"}/>) : <Divider/>}
      <form onSubmit={this.resetAndSearch}>
        <TextField
            id="searchParam"
            onChange={this.searchDidChange}
            value={this.state.searchParam}
            label="Enter name"
            className={classes.textField}
            margin="normal"
            InputProps={{
              startAdornment: (
                  <InputAdornment position="start">
                    <Search/>
                  </InputAdornment>
              ),
            }}
        />
      </form>
      <Button className={classes.button} variant="raised" color="primary" onClick={this.resetAndSearch}>Search</Button>
      <br/>
      <Divider/>
      <br/>
      {this.renderResponse()}
      {(enabledNext || enabledPrevious) && <div>
        <Typography>Page {(response.number + 1) + " of " + response.totalPages}  </Typography>
        <Divider/>
      </div>}
      <Button className={classes.button}
              variant="raised"
              color="primary"
              disabled={!enabledPrevious}
              onClick={this.doPrevious}>Previous</Button>
      <Button className={classes.button}
              variant="raised"
              color="primary"
              disabled={!enabledNext}
              onClick={this.doNext}>Next</Button>
    </div>
  }
}

SearchPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchPage);