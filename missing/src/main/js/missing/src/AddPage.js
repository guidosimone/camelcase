import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import 'react-notifications/lib/notifications.css';

import Typography from "@material-ui/core/es/Typography/Typography";
import Button from "@material-ui/core/es/Button/Button";
import {TextField} from "@material-ui/core/es/index";
import FormControl from "@material-ui/core/es/FormControl/FormControl";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";
import Select from "@material-ui/core/es/Select/Select";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import Divider from "@material-ui/core/es/Divider/Divider";
import LinearProgress from "@material-ui/core/es/LinearProgress/LinearProgress";
import {NotificationContainer, NotificationManager} from 'react-notifications';


const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  select: {
    width: 200,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  menu: {
    width: 200,
  },
});



class AddPage extends React.Component {
  state = {
    waiting: false,
    person : {
      image:"",
      firstName: "",
      lastName: "",
      address: "",
      gender: "",
      dateOfBirth: null,
      race: "",
      hairColor: "",
      eyeColor: "",
      height: "",
      weight: "",
      missingSince: null,
      agency: null,
      contactPhone: "",
    }
  };
  requiredFields = ["firstName", "lastName", "gender", "contactPhone"];
  imageDidChange = (e) => {
	  	console.log(e.target.value);
	    this.personDidChange(e);
	  };
  personDidChange = (e) => {
    const newState = Object.assign({}, this.state);
    const id = e.target.id || e.target.name;
    newState.person[id] = e.target.value;
    this.setState(newState);
  };
  doAdd = () => {
    console.log("doAdd");
    const url = "http://" + window.location.host + "/person";

    this.setState({waiting: true})
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(this.state.person),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }).then(res => res.json())
        .catch(error => {
          this.setState({waiting: false})
          console.error('Error:', error)
        })
        .then(response => {
        				 console.log('Success:', response);
        				 const person = {
        						 image:"",
        			    	      firstName: "",
        			    	      lastName: "",
        			    	      address: "",
        			    	      gender: "",
        			    	      dateOfBirth: null,
        			    	      race: "",
        			    	      hairColor: "",
        			    	      eyeColor: "",
        			    	      height: "",
        			    	      weight: "",
        			    	      missingSince: null,
        			    	      agency: null,
        			    	      contactPhone: "",
        			    	  };

        			    this.setState({person, waiting: false});
        			    NotificationManager.success('Success', 'Missing Person Added');

        			}
        		);
  };

  render() {
    const {classes} = this.props;
    const disabled = Boolean(this.requiredFields.find((name) => {
      return !this.state.person[name]
    }));
    return <form>
      <NotificationContainer/>
      <Typography variant={"headline"}>Add Missing Person</Typography>
      {this.state.waiting ? (<LinearProgress variant={"query"}/>) : <Divider/>}
      <TextField fullWidth required
			      id="image"
			      onChange={this.personDidChange}
			      value={this.state.person.image}
			      label=""
			      className={classes.textField}
      			  type="file"
			      margin="normal"/>
      	 <TextField fullWidth required
	    		id="contactPhone"
	    		onChange={this.personDidChange}
	    		value={this.state.person.contactPhone}
	    		label="Contact Phone"
	    		className={classes.textField}
	    		margin="normal"/>
      <TextField fullWidth required
                 id="firstName"
                 onChange={this.personDidChange}
                 value={this.state.person.firstName}
                 label="First Name"
                 className={classes.textField}
                 margin="normal"/>
      <TextField fullWidth required
                 onChange={this.personDidChange}
                 value={this.state.person.lastName}
                 id="lastName"
                 label="Last Name"
                 className={classes.textField}
                 margin="normal"/>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="gender">Gender *</InputLabel>
        <Select required
            value={this.state.person.gender}
            onChange={this.personDidChange}
            inputProps={{
              name: 'gender',
              id: 'gender'
            }}
            className={classes.select}
        >
          <MenuItem value="">
            <em>Not specified</em>
          </MenuItem>
          <MenuItem value={'Male'}>Male</MenuItem>
          <MenuItem value={'Female'}>Female</MenuItem>
        </Select>
      </FormControl>
      <TextField fullWidth
                 value={this.state.person.address}
                 id="address"
                 label="Address"
                 onChange={this.personDidChange}	 
                 className={classes.textField}
                 margin="normal"/>
       <TextField
       				value={this.state.person.dateOfBirth}
                     id="dateofBirth"
                     label="Date of Birth"
                     type="date"
                     defaultValue="1900-01-01"
                     className={classes.textField}
      				 margin="normal" />
      					<br/>
      <FormControl className={classes.formControl}>
      		        <InputLabel htmlFor="race">Race</InputLabel>
      		        <Select
      		            value={this.state.person.race}
      		            onChange={this.personDidChange}
      		            inputProps={{
      		              name: 'race',
      		              id: 'race'
      		            }}
      		            className={classes.select}>
      		          <MenuItem value="">
      		            <em>Not specified</em>
      		          </MenuItem>
      		          <MenuItem value={'I'}>American Indian or Alaskan Native</MenuItem>
      		          <MenuItem value={'A'}>Asian or Pacific Islander</MenuItem>
      		          <MenuItem value={'B'}>Black</MenuItem>
      		          <MenuItem value={'W'}>White</MenuItem>
      		          <MenuItem value={'U'}>Unknown</MenuItem>
      		        </Select>
      		     </FormControl>
                <br/>
      <FormControl className={classes.formControl}>
      		        <InputLabel htmlFor="hairColor">Hair Color</InputLabel>
      		        <Select
      		            value={this.state.person.hairColor}
      		            onChange={this.personDidChange}
      		            inputProps={{
      		              name: 'hairColor',
      		              id: 'hairColor'
      		            }}
      		            className={classes.select}>
      		          <MenuItem value="">
      		            <em>Not specified</em>
      		          </MenuItem>
      		          <MenuItem value={'Bald'}>Bald</MenuItem>
      		          <MenuItem value={'Black'}>Black</MenuItem>
      		          <MenuItem value={'Blond or Strawberry'}>Blond or Strawberry</MenuItem>
      		          <MenuItem value={'Blue'}>Blue</MenuItem>
      		          <MenuItem value={'Brown'}>Brown</MenuItem>
      		          <MenuItem value={'Gray or Partially Gray'}>Gray or Partially Gray</MenuItem>
      		          <MenuItem value={'Green'}>Green</MenuItem>
      		          <MenuItem value={'Orange'}>Orange</MenuItem>
      		          <MenuItem value={'Pink'}>Pink</MenuItem>
      		          <MenuItem value={'Purple'}>Purple</MenuItem>
      		          <MenuItem value={'Red or Auburn'}>Red or Auburn</MenuItem>
      		          <MenuItem value={'Sandy'}>Sandy</MenuItem>
      		          <MenuItem value={'White'}>White</MenuItem>
      		          <MenuItem value={'Unknown'}>Unknown</MenuItem>

      		        </Select>
      		     </FormControl>
      					 <br/>
      <FormControl className={classes.formControl}>
      		        <InputLabel htmlFor="eyeColor">Eye Color</InputLabel>
      		        <Select
      		            value={this.state.person.eyeColor}
      		            onChange={this.personDidChange}
      		            inputProps={{
      		              name: 'eyeColor',
      		              id: 'eyeColor'
      		            }}
      		            className={classes.select}>
      		          <MenuItem value="">
      		            <em>Not specified</em>
      		          </MenuItem>
      		          <MenuItem value={'Black'}>Black</MenuItem>
      		          <MenuItem value={'Blue'}>Blue</MenuItem>
      		          <MenuItem value={'Brown'}>Brown</MenuItem>
      		          <MenuItem value={'Green'}>Green</MenuItem>
      		          <MenuItem value={'Maroon'}>Maroon</MenuItem>
      		          <MenuItem value={'Pink'}>Pink</MenuItem>
      		          <MenuItem value={'Gray'}>Gray</MenuItem>
      		          <MenuItem value={'Hazel'}>Hazel</MenuItem>
      		          <MenuItem value={'Multicolored'}>Multicolored</MenuItem>>
      		          <MenuItem value={'Unknown'}>Unknown</MenuItem>

      		        </Select>
      		     </FormControl>
      			<TextField fullWidth
      			    	id="height"
      			    		onChange={this.personDidChange}
      			    	value={this.state.person.height}
      			    	label="Height"
      			    	className={classes.textField}
      			    	margin="normal"/>
          	 <TextField fullWidth
          			    	id="weight"
          			    		onChange={this.personDidChange}
          			    	value={this.state.person.weight}
          			    	label="Weight"
          			    	className={classes.textField}
          			    	margin="normal"/>
      			<br/>
      			<br/>
      <Button disabled={disabled} variant="raised" color="primary" onClick={this.doAdd}>Add</Button>
    </form>

  }
}

AddPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddPage);