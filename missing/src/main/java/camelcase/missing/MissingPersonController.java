package camelcase.missing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import camelcase.missing.cad.CADRestApiService;
import camelcase.missing.twitter.TwitterService;

@RestController
@RequestMapping("/person")
public class MissingPersonController
{
	private final Logger			log			= LoggerFactory.getLogger( getClass() );

	List<PersonDTO>					personList	= new ArrayList<PersonDTO>();

	private CADRestApiService		cadRestApiService;
	private MissingPersonService	missingPersonService;
	private TwitterService			twitterService;

	public MissingPersonController(CADRestApiService cadRestApiService, MissingPersonService missingPersonService, TwitterService twitterService)
	{
		this.cadRestApiService = cadRestApiService;
		this.missingPersonService = missingPersonService;
		this.twitterService = twitterService;
	}

	@PostMapping
	public ResponseEntity<?> save( @RequestBody PersonDTO personDTO )
	{
		personDTO.setId( UUID.randomUUID().toString() );
		personDTO.setMissingSince( new Date() );
		cadRestApiService.savePerson( personDTO );
		twitterService.sendTwitterUpdate( personDTO );
		missingPersonService.savePerson( personDTO );

		log.info( "Total save successful" );

		return ResponseEntity.accepted().build();
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Page<PersonDTO>> getPersons(@RequestParam(value="searchParam", required = false) String searchParam, @RequestParam( "page" ) int page)
	{
		List<PersonDTO> persons = missingPersonService.getPersons( searchParam );
		page = page < 0 ? 0 : page;

        List<PersonDTO> subItems = persons.subList(10*page, Math.min( (10*page)+10, persons.size()));
		PageRequest pr = PageRequest.of(page, 10);
		Page results = new PageImpl<>(subItems, pr, persons.size());

		return ResponseEntity.ok().body( results );
	}
}
