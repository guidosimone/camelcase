package camelcase.missing.twitter;

import java.io.File;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import camelcase.missing.PersonDTO;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

@Service
public class TwitterService {

	public void sendTwitterUpdate(String message) {
		try {
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
			AccessToken accessToken = new AccessToken(TwitterConstants.ACCESS_TOKEN,TwitterConstants.ACCESS_TOKEN_SECRET);		
			twitter.setOAuthAccessToken(accessToken);
			twitter.updateStatus(message);

			System.out.println("Successfully updated the status in Twitter.");
		} catch (TwitterException te) {
			te.printStackTrace();
		}
	}
	
	public void sendTwitterUpdate(PersonDTO person) {
		try {
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(TwitterConstants.CONSUMER_KEY, TwitterConstants.CONSUMER_SECRET);
			AccessToken accessToken = new AccessToken(TwitterConstants.ACCESS_TOKEN,TwitterConstants.ACCESS_TOKEN_SECRET);		
			twitter.setOAuthAccessToken(accessToken);
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm");

            String statusMessage = "Name: " + person.getFirstName() + " " + person.getLastName()
                                    + "\n" + "Gender: " + person.getGender()
                                    + "\n" + "Missing Since: " + format.format(person.getMissingSince())
                                    + "\n" + "Contact #: " + person.getContactPhone();
			
			StatusUpdate status = new StatusUpdate(statusMessage);
			
			if(StringUtils.isNotBlank(person.getImage())) {
				File file = new File(person.getImage()); 
				status.setMedia(file); 
			}
			
			twitter.updateStatus(status);
			
			
			
			System.out.println("Successfully updated the status in Twitter.");
		} catch (TwitterException te) {
			te.printStackTrace();
		}
	}
	
	
	
	
	
	
}
