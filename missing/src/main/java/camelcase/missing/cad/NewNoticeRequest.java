package camelcase.missing.cad;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewNoticeRequest
{
	private String	reason;
	private Date	startTime;
	private Date	cancellationTime;
	private String	noticeType;
	private String	unitOrg;
	private String	firstName;
	private String	lastName;
	private String	mapPoints;

	public String getReason()
	{
		return reason;
	}

	public void setReason( String reason )
	{
		this.reason = reason;
	}

	@JsonProperty(value = "start-time")
	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime( Date startTime )
	{
		this.startTime = startTime;
	}

	@JsonProperty(value = "cancellation-time")
	public Date getCancellationTime()
	{
		return cancellationTime;
	}

	public void setCancellationTime( Date cancelationTime )
	{
		this.cancellationTime = cancelationTime;
	}

	@JsonProperty(value = "notice-type")
	public String getNoticeType()
	{
		return noticeType;
	}

	public void setNoticeType( String noticeType )
	{
		this.noticeType = noticeType;
	}

	@JsonProperty(value = "unit-org")
	public String getUnitOrg()
	{
		return unitOrg;
	}

	public void setUnitOrg( String unitOrg )
	{
		this.unitOrg = unitOrg;
	}

	@JsonProperty(value = "first-name")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}

	@JsonProperty(value = "last-name")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}

	@JsonProperty(value = "map-points")
	public String getMapPoints()
	{
		return mapPoints;
	}

	public void setMapPoints( String mapPoints )
	{
		this.mapPoints = mapPoints;
	}
}
