package camelcase.missing.cad;

public class CADRestLoginRequest
{
	private String	username;
	private String	password;
	private Boolean	isHashedPassword;

	public String getUsername()
	{
		return username;
	}

	public void setUsername( String username )
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword( String password )
	{
		this.password = password;
	}

	public Boolean getIsHashedPassword()
	{
		return isHashedPassword;
	}

	public void setIsHashedPassword( Boolean isHashedPassword )
	{
		this.isHashedPassword = isHashedPassword;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "CADRestLoginRequest [" );
		if (username != null)
			builder.append( "username=" ).append( username );
		builder.append( "]" );
		return builder.toString();
	}

}
