package camelcase.missing.cad;

import java.net.URI;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import camelcase.missing.PersonDTO;

@Service
public class CADRestApiService implements InitializingBean
{
	private final Logger					log							= LoggerFactory.getLogger( getClass() );

	protected ObjectMapper					objectMapper;

	private CloseableHttpClient				httpClient;

	private String							cadRestApiBase;
	private String							ssoToken;

	public static final String RESTAPI_DEFAULT = "http://127.0.0.1:";
	public static final String AUTHENTICATION = "/api/v1/auth";

	public CADRestApiService(ObjectMapper objectMapper)
	{
		this.objectMapper = objectMapper;
	}
	
	public void savePerson( PersonDTO personDTO )
	{
		log.info( "Saving Person {}", personDTO );
		
		
		NewNoticeRequest noticeRequest = new NewNoticeRequest();
		noticeRequest.setUnitOrg( "Hancock County" );
		noticeRequest.setReason("Missing Person: " + personDTO.getFirstName() + " " + personDTO.getLastName());
		noticeRequest.setCancellationTime( null );
		noticeRequest.setStartTime( new Date() );
		noticeRequest.setNoticeType( "MISSING PERSON" );
		noticeRequest.setFirstName( personDTO.getFirstName() );
		noticeRequest.setLastName( personDTO.getLastName() );
		noticeRequest.setMapPoints("Missing Person: " + personDTO.getFirstName() + " " + personDTO.getLastName());
		
		try
		{

			postAsync(  "/api/v1/cad/notices", noticeRequest, null );
		}
		catch (CADRestException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends authentication request to CAD Rest API and on successful authentication stores the sso token in the SSO_TOKEN field.
	 * @return - boolean indicating whether or not authentication was successful
	 * @throws Exception
	 */
	private boolean authenticateWithCadRestApi() throws Exception
	{
		CADRestLoginRequest loginRequest = new CADRestLoginRequest();
		String username = "superuser@Default";
		String password = "interact!";

		loginRequest.setUsername( username );
		loginRequest.setPassword( password );
		loginRequest.setIsHashedPassword( false );
		String writeValueAsString = objectMapper.writeValueAsString( loginRequest );

		HttpEntity requestPostBodyEntity = new StringEntity( writeValueAsString );
		String authUrl = StringUtils.join( cadRestApiBase, AUTHENTICATION );

		log.debug( "Authenticating with CAD Rest API at {} with {}", authUrl, loginRequest );

		HttpUriRequest login = RequestBuilder //
				.post() //
				.setUri( new URI( authUrl ) ) //
				.setEntity( requestPostBodyEntity ) //
				.setHeader( HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE ) //
				.setHeader( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE ) //
				.build();

		try (CloseableHttpResponse response = httpClient.execute( login ))
		{
			StatusLine statusLine = response.getStatusLine();
			log.debug( "Authentication StatusLine: {}", statusLine );
			if (statusLine.getStatusCode() == 200)
			{
				HttpEntity responsePostBodyEntity = response.getEntity();
				String string = EntityUtils.toString( responsePostBodyEntity );

				CADRestLoginResponse cadRestLoginResponse = objectMapper.readValue( string, CADRestLoginResponse.class );
				ssoToken = cadRestLoginResponse.getSsoTokenId();

				return true;
			}
			else
			{
				log.warn( "Unexpected response code while authenticating user with CAD REST API: {}", statusLine );
				return false;
			}
		}
		catch (Exception e)
		{
			throw e;
		}

	}

	/**
	 * Useful for sending post request to CAD Rest API where we don't care what's in the response body. If we need the response body 
	 * then the non-async post method should be used.
	 * @param path
	 * @param body
	 * @param type
	 * @throws Exception
	 */
	@Async
	private <T> void postAsync( String path, Object body, Class<T> type ) throws CADRestException
	{
		post( path, body, type );
	}

	/**
	 * 
	 * @param path - the relative url we're sending the post request to
	 * @param body - the object we want in the post request body
	 * @param type - the class type expected back in the response body
	 * @return post response body
	 * @throws Exception
	 */
	private <T> T post( String path, Object body, Class<T> type ) throws CADRestException
	{
		T t = null;

		String jsonBody;

		String url = StringUtils.join( cadRestApiBase, path );
		try
		{
			jsonBody = objectMapper.writeValueAsString( body );
			log.debug( "POSTing to {} with Body: {}", url, jsonBody );

			HttpEntity entity = new StringEntity( jsonBody, ContentType.APPLICATION_JSON );
			RequestBuilder requestBuilder = RequestBuilder.post( url );
			
			if(StringUtils.isNotBlank( ssoToken ))
			{
				requestBuilder.addHeader( HttpHeaders.AUTHORIZATION, ssoToken );
			}

			requestBuilder.setEntity( entity );
			try (CloseableHttpResponse response = httpClient.execute( requestBuilder.build() ))
			{
				log.debug( "POST request sent. Response: {}", response.getStatusLine() );
				
				if(response.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR)
				{
					String message = "CAD Rest API returned unexpected response code " + HttpStatus.SC_INTERNAL_SERVER_ERROR;
					throw new CADRestException(message, response.getStatusLine());
				}
				
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED || response.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN)
				{
					if (authenticateWithCadRestApi())
					{
						post( path, body, type );
					}
				}

				if (type != null)
				{
					HttpEntity responseEntity = response.getEntity();
					String content = EntityUtils.toString( responseEntity );
					log.info( "Content returned from post {}: {}", url, content );
					t = objectMapper.readValue( content, type );
				}

			}
			catch (Exception e)
			{
				String message = "An error occurred while executing HTTP post at URL: " + url + ", " + e.getMessage();
				
				if(e instanceof CADRestException)
				{
					CADRestException cre = (CADRestException) e;
					throw new CADRestException(message, cre.getStatusLine());
				}
				
			}
		}
		catch (JsonProcessingException e)
		{
			log.warn( "Could not convert string to json formatted value {}", body, e );
		}

		return t;
	}

	@Override
	public void afterPropertiesSet() throws Exception
	{
		cadRestApiBase = StringUtils.join( RESTAPI_DEFAULT, "9002" );

		httpClient = HttpClients.custom().disableRedirectHandling().build();
	}

	public String getCadRestApiBase()
	{
		return cadRestApiBase;
	}

	public void setCadRestApiBase( String cadRestApiBase )
	{
		this.cadRestApiBase = cadRestApiBase;
	}

	public String getSsoToken()
	{
		return ssoToken;
	}

	public void setSsoToken( String ssoToken )
	{
		this.ssoToken = ssoToken;
	}
}
