package camelcase.missing.cad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CADRestLoginResponse
{
	private String	ssoTokenId;
	private String	expirationDate;
	private String	tenantId;
	private String	tenantName;
	private String	domainId;
	private String	iaUserId;

	public String getSsoTokenId()
	{
		return ssoTokenId;
	}

	public void setSsoTokenId( String ssoTokenId )
	{
		this.ssoTokenId = ssoTokenId;
	}

	public String getExpirationDate()
	{
		return expirationDate;
	}

	public void setExpirationDate( String expirationDate )
	{
		this.expirationDate = expirationDate;
	}

	public String getTenantId()
	{
		return tenantId;
	}

	public void setTenantId( String tenantId )
	{
		this.tenantId = tenantId;
	}

	public String getTenantName()
	{
		return tenantName;
	}

	public void setTenantName( String tenantName )
	{
		this.tenantName = tenantName;
	}

	public String getDomainId()
	{
		return domainId;
	}

	public void setDomainId( String domainId )
	{
		this.domainId = domainId;
	}

	public String getIaUserId()
	{
		return iaUserId;
	}

	public void setIaUserId( String iaUserId )
	{
		this.iaUserId = iaUserId;
	}

}
