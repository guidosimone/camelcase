package camelcase.missing.cad;

import org.apache.http.StatusLine;

public class CADRestException extends Exception
{

	private StatusLine statusLine;
	
	public CADRestException() {}

	public CADRestException(String message)
	{
		super(message);
	}
	
	public CADRestException(String message, StatusLine statusLine)
	{
		super(message);
		this.statusLine = statusLine;
	}
	
	public StatusLine getStatusLine()
	{
		return statusLine;
	}

	public void setStatusLine( StatusLine statusLine )
	{
		this.statusLine = statusLine;
	}
}
