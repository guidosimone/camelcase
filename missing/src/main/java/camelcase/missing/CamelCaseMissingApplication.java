package camelcase.missing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelCaseMissingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelCaseMissingApplication.class, args);
	}
}
