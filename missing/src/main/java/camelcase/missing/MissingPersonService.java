package camelcase.missing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

@Service
public class MissingPersonService implements InitializingBean
{
	private final Logger	log		= LoggerFactory.getLogger( getClass() );

	List<PersonDTO>			persons	= new ArrayList<PersonDTO>();

	@Override
	public void afterPropertiesSet() throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = MissingPersonService.class.getResourceAsStream( "persons.json" );
		TypeFactory typeFactory = mapper.getTypeFactory();

		persons = mapper.readValue( is, typeFactory.constructCollectionType( List.class, PersonDTO.class ) );
	}

	public void savePerson( PersonDTO personDTO )
	{
		String image = encodeImage( personDTO.getImage() );
		personDTO.setImage( image );
		persons.add( personDTO );
		log.info( "Saved person in 'database'" );
	}

	private String encodeImage( String imagePath )
	{
		String base64Image = "";
		File file = new File( imagePath );
		try (FileInputStream imageInFile = new FileInputStream( file ))
		{
			// Reading a Image file from file system
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read( imageData );
			base64Image = Base64.getEncoder().encodeToString( imageData );
		}
		catch (FileNotFoundException e)
		{
			System.out.println( "Image not found" + e );
		}
		catch (IOException ioe)
		{
			System.out.println( "Exception while reading the Image " + ioe );
		}
		return "data:image/jpeg;base64," + base64Image;
	}

	public List<PersonDTO> getPersons()
	{
		sortPersons();
		return persons;
	}

	public List<PersonDTO> getPersons( String searchParam )
	{
		if(StringUtils.isEmpty( searchParam ))
		{
			return getPersons();
		}
		sortPersons();
		String param = searchParam.toLowerCase();

		Stream<PersonDTO> stream = persons.stream().filter( new Predicate<PersonDTO>() {
			@Override
			public boolean test( PersonDTO p )
			{
				String[] split = StringUtils.split( param, " ," );
				for (String value : split)
				{
					if (p.getFirstName().toLowerCase().contains( value ) || p.getLastName().toLowerCase().contains( value ))
					{
						return true;
					}
				}
				return false;
			}
		} );

		Object[] array = stream.toArray();

		List<PersonDTO> matchingPersons = new ArrayList<PersonDTO>();
		for (Object object : array)
		{
			matchingPersons.add( (PersonDTO) object );
		}
		return matchingPersons;
	}

	private void sortPersons()
	{
		Collections.sort( persons, new Comparator<PersonDTO>() {
			public int compare( PersonDTO person1, PersonDTO person2 )
			{
				return person2.getMissingSince().compareTo( person1.getMissingSince() );
			}
		} );
	}

}
