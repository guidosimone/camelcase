package camelcase.missing;

import java.util.Date;

public class PersonDTO
{
	private String	id;
	private String	firstName;
	private String	lastName;
	private String	address;
	private String	gender;
	private Date	dateOfBirth;
	private String	race;
	private String	hairColor;
	private String	eyeColor;
	private int		height;
	private int		weight;
	private Date	missingSince;
	private String	image;
	private String	agency;
	private String	contactPhone;

	public PersonDTO()
	{

	}

	public String getId()
	{
		return id;
	}

	public void setId( String id )
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress( String address )
	{
		this.address = address;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender( String gender )
	{
		this.gender = gender;
	}

	public Date getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth( Date dateOfBirth )
	{
		this.dateOfBirth = dateOfBirth;
	}

	public String getRace()
	{
		return race;
	}

	public void setRace( String race )
	{
		this.race = race;
	}

	public String getHairColor()
	{
		return hairColor;
	}

	public void setHairColor( String hairColor )
	{
		this.hairColor = hairColor;
	}

	public String getEyeColor()
	{
		return eyeColor;
	}

	public void setEyeColor( String eyeColor )
	{
		this.eyeColor = eyeColor;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight( int height )
	{
		this.height = height;
	}

	public int getWeight()
	{
		return weight;
	}

	public void setWeight( int weight )
	{
		this.weight = weight;
	}

	public Date getMissingSince()
	{
		return missingSince;
	}

	public void setMissingSince( Date missingSince )
	{
		this.missingSince = missingSince;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage( String image )
	{
		this.image = image;
	}

	public String getAgency()
	{
		return agency;
	}

	public void setAgency( String agency )
	{
		this.agency = agency;
	}

	public String getContactPhone()
	{
		return contactPhone;
	}

	public void setContactPhone( String contactPhone )
	{
		this.contactPhone = contactPhone;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append( "PersonDTO [id=" ).append( id ).append( ", firstName=" ).append( firstName ).append( ", lastName=" ).append( lastName ).append( ", address=" ).append( address )
				.append( ", gender=" ).append( gender ).append( ", dateOfBirth=" ).append( dateOfBirth ).append( ", race=" ).append( race ).append( ", hairColor=" ).append( hairColor )
				.append( ", eyeColor=" ).append( eyeColor ).append( ", height=" ).append( height ).append( ", weight=" ).append( weight ).append( ", missingSince=" ).append( missingSince )
				.append( ", image=" ).append( image ).append( ", agency=" ).append( agency ).append( ", contactPhone=" ).append( contactPhone ).append( "]" );
		return builder.toString();
	}

}
