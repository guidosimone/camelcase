package camelcase.downloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.reactive.function.client.WebClient;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.util.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Logger logger = LoggerFactory.getLogger(Main.class);
        Map jsonData = getJsonFromFile(new File("/Users/guidosimone/Downloads/king-county-pets.json"));
//        logger.info("Received " + jsonData);
        Map imageColumn = getColumnByName(jsonData, "Image");
        int imageIndex = getColumnIndexByName(jsonData, "Image");
        logger.info("Image column is " + imageColumn);
        logger.info("Position is " + imageIndex);
        List images = getColumn(jsonData, "Image");
        List<String> urls = imageUrls(images);
        logger.info("Images are " + urls);
        if (!urls.isEmpty()) {
            String url = urls.get(0);
            String baseUrl = StringUtils.substringBeforeLast(url,"/").replace("https://", "http://");
            WebClient client = WebClient.create(baseUrl);

            byte [] bytes = client.get()
                    .uri("/" + StringUtils.substringAfterLast(url, "/"))
                    .exchange()
                    .flatMap(clientResponse -> clientResponse.bodyToMono(ByteArrayResource.class))
                    .map(ByteArrayResource::getByteArray)
                    .block();

            String imageData = "data:image/jpg;base64," + DatatypeConverter.printBase64Binary(bytes);
            logger.info(imageData);
        }
    }

    private static List<String> imageUrls(List images) {
        List<String> urls = new ArrayList<>();
        for (Object image : images) {
            if (image instanceof List) {
                List image2 = (List) image;
                if (image2.size() == 2) {
                    Object url = image2.get(0);
                    if (url instanceof String) {
                        urls.add((String) url);
                    }
                }
            }
        }
        return urls;
    }

    private static Map getJsonFromFile(File file) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(file, Map.class);
    }

    static Map getView(Map jsonData) {

        return getMap(getMap(jsonData, "meta"), "view");
    }

    static List<Map> getColumns(Map jsonData) {
        Map view = getView(jsonData);

        Object columns = view.get("columns");
        if (! (columns instanceof List))
            return Collections.emptyList();

        return (List<Map>) columns;
    }

    static String getString(Map map, String key) {
        Object value = map.get(key);
        if (! (value instanceof String))
            return "";
        return (String) value;
    }
    static Integer getInteger(Map map, String key) {
        Object value = map.get(key);
        if (! (value instanceof Integer))
            return -1;
        return (Integer) value;
    }
    static Map getMap(Map map, String key) {
        Object value = map.get(key);
        if (! (value instanceof Map))
            return Collections.emptyMap();
        return (Map) value;
    }
    static List getList(Map map, String key) {
        Object value = map.get(key);
        if (! (value instanceof List))
            return Collections.emptyList();
        return (List) value;
    }

    static Map getColumnByName(Map jsonData, String name) {
        List<Map> columns = getColumns(jsonData);

        Map result = null;
        for (Map column : columns) {
            if (getString(column, "name").equalsIgnoreCase(name)) {
                result = column;
                break;
            }
        }
        return result == null ? Collections.emptyMap() : result;
    }
    static int getColumnIndexByName(Map jsonData, String name) {
        List<Map> columns = getColumns(jsonData);

        int result = -1;
        for (int index = 0; index < columns.size(); index++) {
            Map column = columns.get(index);
            if (getString(column, "name").equalsIgnoreCase(name)) {
                result = index;
                break;
            }
        }
        return result;
    }

    static List<String> getColumn(Map jsonData, String columnName) {
        int position = getColumnIndexByName(jsonData, columnName);
        if (position < 0)
            return Collections.emptyList();

        List result = new ArrayList<>();
        List rows = getList(jsonData, "data");
        for (Object row : rows) {
            if (! (row instanceof List))
                continue;
            List cells = (List) row;
            if (cells.size() <= position)
                continue;
            Object cell = cells.get(position);
            result.add(cell);
        }
        return result;
    }

    private static Map getJsonFromWeb() {
        WebClient client = WebClient.create("https://data.kingcounty.gov");

        return client.get()
                .uri("/api/views/yaai-7frk/rows.json?accessType=DOWNLOAD")
                .retrieve()
                .bodyToMono(Map.class)
                .block();
    }

}
