package camelcase.fibserver;

import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Guido Simone
 */
public class FibUtilsTest {

    @Test
    public void testFibReactor() throws Exception {
        TestSubscriber<Long> test = FibUtils.fibonacciWithDelay(Duration.ofMillis(100))
                .limit(10)
                .test();

        assertThat(test.await(5, TimeUnit.SECONDS)).isTrue();
        assertThat(test.values()).containsExactly(0L, 1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L);
    }
}
