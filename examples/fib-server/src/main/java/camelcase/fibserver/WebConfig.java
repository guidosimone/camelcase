package camelcase.fibserver;

import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static camelcase.fibserver.FibUtils.fibonacciWithDelay;
import static org.apache.logging.log4j.util.Strings.isNotBlank;

/**
 * Configure any web service handlers for the application.
 * <p/>
 * Based on sample code at:
 * https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html#webflux-websocket-server-handler
 */
@Configuration
public class WebConfig implements WebFluxConfigurer  {
    private static final Logger logger = LoggerFactory.getLogger(WebConfig.class);

    /**
     * Register any websocket handlers with a URI path
     */
    @Bean
    public HandlerMapping handlerMapping() {
        Map<String, WebSocketHandler> map = new HashMap<>();

        // The fibonacci handler works with a URL like:
        // http://localhost:8080/wsfib?count=10&delay=250
        map.put("/wsfib", new WSFibonacci());

        SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
        mapping.setUrlMap(map);
        mapping.setOrder(-1); // before annotated controllers
        return mapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

    public static class WSFibonacci implements WebSocketHandler {
        @Override
        public Mono<Void> handle(WebSocketSession session) {
            logger.info("New Websocket session at " + session.getHandshakeInfo());

            // parse the URI for count/delay parameters
            UriComponents uri = UriComponentsBuilder.fromUri(session.getHandshakeInfo()
                    .getUri())
                    .build();
            MultiValueMap<String, String> params = uri.getQueryParams();
            int count = Integer.parseInt(params.getFirst("count"));
            int delay = Integer.parseInt(params.getFirst("delay"));
            String operator = params.getFirst("operator");
            logger.info("Begin fibonacci sequence of " + count + " numbers with delay of " + delay + "ms");

            // Create a fibonacci stream (of numbers) and convert it to a stream of websocket
            // text messages
            Flowable<Long> fibStream = fibonacciWithDelay(Duration.ofMillis(delay))
                    .limit(count)
                    .cache();
            Flowable<Long> finalStream = fibStream;
            if (isNotBlank(operator)) {
                switch (operator) {
                    case "negate":
                        finalStream = fibStream.map(n -> -1 * n);
                        break;
                    case "concat":
                        finalStream = Flowable.concat(fibStream, fibStream.map(n -> -1 * n));
                        break;
                    case "merge":
                        finalStream = Flowable.merge(fibStream, fibStream.map(n -> -1 * n));
                        break;
                }

            }
            Flowable<WebSocketMessage> messages = finalStream
                    .map(aLong -> session.textMessage("" + aLong));

            // Send the stream of messages to the client.  More precisely, return a Mono<Void>
            // which, when subscribed, will start sending the websocket messages to the client.
            // When the websocket stream ends, the returned Mono will complete, causing the
            // websocket connection to terminate.
            return session.send(messages);
        }
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/static/**")
//                .addResourceLocations("/static");
//    }
}