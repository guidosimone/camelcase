package camelcase.fibserver;

import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * @author Guido Simone
 */
@SuppressWarnings("WeakerAccess")
public class FibUtils {
    public static Flowable<Long> fibonacciWithDelay(Duration duration) {
        return fibonacci()
                .concatMapSingle(n -> Single.just(n).delay(duration.getNano(), TimeUnit.NANOSECONDS));
    }
    public static Flowable<Long> fibonacci() {
        return Flowable.generate(() -> null, (Pair p, Emitter<Long> longEmitter) -> {
            Pair p2;
            if (p == null) {
                p2 = new Pair(0, 1);
            } else {
                p2 = new Pair(p.n2, p.n2 + p.n1);
            }
            longEmitter.onNext(p2.n1);
            return p2;
        });
    }

    static class Pair {
        final long n1;
        final long n2;

        Pair(long n1, long n2) {
            this.n1 = n1;
            this.n2 = n2;
        }
    }
}
