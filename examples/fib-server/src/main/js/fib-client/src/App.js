import React, {Component} from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: "Idle",
      count: 10,
      delay: 250,
      operator: "identity",
      items: []
    }
  }

  countDidChange = (e) => {
    console.log("countDidChange");
    this.setState({
      count: Number(e.target.value)
    });
  };
  delayDidChange = (e) => {
    console.log("delayDidChange");
    this.setState({
      delay: Number(e.target.value)
    });
  };
  operatorDidChange = (e) => {
    console.log("operatorDidChange");
    this.setState({
      operator: e.target.value
    });
  };

  start = (ev) => {
    console.log("Start fib ws");
    this.setState({
      items: []
    });
    const self = this;
    const loc = window.location;
    const ws = new WebSocket("ws://" + loc.host + "/wsfib" +
        "?count=" + this.state.count +
        "&delay=" + this.state.delay +
        "&operator=" + this.state.operator);

    ws.onopen = function (event) {
      console.log('open');
      self.setState({
        status: "WebSocket connection established"
      })
    };

    ws.onerror = function (arg) {
      console.log('error');

    };
    ws.onclose = function (arg) {
      console.log('close');
      self.setState({
        status: "WebSocket connection closed"
      })

    };

    ws.onmessage = function (event) {
      const newItems = [event.data].concat(self.state.items);
      self.setState({
        status: "WebSocket connection receiving...",
        items: newItems
      });
    };
  };

  render() {
    return (
        <div className="App">
          <h1>Example of Spring Reactive Web using WebSockets</h1>
          <p>{this.state.status}</p>
          <button onClick={this.start}>Start Fibonacci Sequence</button>
          <p/>
          <table>
            <tr>
              <td><label>Count: </label></td>
              <td>
                <select value={this.state.count} onChange={this.countDidChange}>
                  <option value={10}>10 numbers</option>
                  <option value={20}>20 numbers</option>
                  <option value={50}>50 numbers</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><label>Delay (ms): </label></td>
              <td>
                <select value={this.state.delay} onChange={this.delayDidChange}>
                  <option value={1}>1 milliseconds</option>
                  <option value={10}>10 milliseconds</option>
                  <option value={100}>100 milliseconds</option>
                  <option value={250}>250 milliseconds</option>
                  <option value={1000}>1 second</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><label>Operator: </label></td>
              <td>
                <select value={this.state.operator} onChange={this.operatorDidChange}>
                  <option value="identity">Identity</option>
                  <option value="negate">Negate</option>
                  <option value="concat">Negate and concat</option>
                  <option value="merge">Negate and merge</option>
                </select>
              </td>
            </tr>
          </table>

          <p/>
          <label>Sequence: </label> {this.state.items.map((item, index) => <span
            key={index}>{item} </span>)}
        </div>
    );
  }
}

export default App;
