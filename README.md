# Fibonacci Samples

The following instructions assume you cloned the camelcase repository to
`c:\projects\camelcase`.  If the project is elsewhere, adjust accordingly.

## Fibonacci Client (fib-client)

The Fibonacci Client is a React JS web application which connects to the Fibonacci Server via
web sockets and displays the Fibonacci stream in real time as the numbers arrive.

The code is embedded in the server directory so that the static resources can be served
by the web server.  

### Prerequisites
 - Node JS  (32 or 64 bit)
 
 You must have your path and environment variables configured such that you can execute
 "npm" (Node Package Manager) from the command line.
 
 The first thing you have to do is download the necessary nodejs packages.
 
```cmd
cd \projects\camelcase\examples\fib-server\src\main\js\fib-client
npm install
```
 
 To run the client in development mode:
 
```cmd
npm start
```

This starts a react development web server which serves the client application.  It also launches
a web browser (Chrome recommended) which connects to this development web server.  This
environment allows for rapid development because it detects any changes in the JavaScript source
and instantly hot deploys the changes.

The react web server has a proxy feature which allows the web client to make web socket connections
directly to the react web server (avoid CORS conflicts that might occur if the client attempts
to connect to port 8080 directly).  The default configuration is in the package.json file
and looks something like this:

```json
 "proxy": {
    "/": {
      "target": "http://localhost:8080"
    },
    "/wsfib": {
      "target": "ws://localhost:8080",
      "ws": true
    }
  }
```
If you run fib-server on a different port, you will need to update this proxy.

To do a production build of fib-client:

```cmd
npm run build
```

It prints out instructions for now to integrate the mini-fied app into a web server. 
The server build takes care of this step automatically as part of the maven build.

## Fibonacci Server (fib-server)
The Fibonacci Server is a Spring Boot application using Spring Boot 2.x and Spring Framework 5.x.
The server provides a Fibonacci sequence as a stream of text messages over a websocket
connection.  The stream of messages has an artificial time delay to make it easier to see
the messages as they arrive over time.

### Prerequisites:
 - Java 8 or greater (32 or 64 bit)
 - Maven  (some reasonably current version)

Also you must have your path and environment variables configured such that you can execute 
"mvn" and "java" from the command line.

To build and run the Fibonacci web service:
```cmd
cd \projects\camelcase\examples\fib-server
mvn install
java -jar target\fib-server-0.0.1-SNAPSHOT.jar
```

This will run the server in the console window.  It uses port 8080 by default - so if you have
Tomcat or MobileGovernment running on your system, the server may fail when it attempts to
bind.  If this is the case, either stop the conflicting server or choose another port:

```cmd
java -jar target\fib-server-0.0.1-SNAPSHOT.jar --server.port=9999
```

